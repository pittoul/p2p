#!/usr/bin/env python3
# coding: utf-8
import socket

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# client.connect(('192.168.0.37', 8081))
client.connect(('82.65.101.145', 8081))
# client.connect(('77.132.37.95', 8082))

output = input("Le serveur pense à un chiffre... lequel ?  ") 
# output = '\nMessage venu du CLIENT Yeaaaah '
client.sendall(output.encode('utf-8'))

from_server = client.recv(4096)

client.close()

print (from_server)
